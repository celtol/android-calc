import React, { Component } from "react";
import {Alert, AppRegistry, StyleSheet, Text, View, ToastAndroid } from "react-native";
import { Button } from "react-native-elements";
import DoubleClick from 'react-native-double-click';
import Icon from 'react-native-vector-icons/Ionicons';

export default class CalculatorScreen extends React.Component {   
	constructor() {
      super()
      this.state = {
         myCalc: "0",
         number1: 0,
         number2: 0,
         action: 0,
         memory: 0,
         dotcount: 0,
         score: 0
      }
      this.handleDblClick = this.handleDblClick.bind(this);
   	}


	static navigationOptions = {
		header: null
	};
  handleDblClick = () => {
    ToastAndroid.show('Wykonał Jonatan Dziedzic', ToastAndroid.SHORT);
  }
  	_but1 = () => {
      if (this.state.myCalc.length == 16) {
        ToastAndroid.show('Przekraczasz zakres wyświetlacza', ToastAndroid.SHORT);
      }
  		else if (this.state.score == 1) {
  			this.setState({
    			myCalc: "1",
    			score: 0
    		})
  		}
      else if (this.state.myCalc != 0 || this.state.myCalc.length >=2) {
        var numb1 = this.state.myCalc;
        var numb2 = "1";
        var numb3 = numb1 + numb2;
        this.setState({
          myCalc: numb3
        });
      }
      else if (this.state.myCalc == 0) {
        this.setState({
          myCalc: "1"
        })
      }
  	}
  	_but2 = () => {
  		if (this.state.myCalc.length == 16) {
        ToastAndroid.show('Przekraczasz zakres wyświetlacza', ToastAndroid.SHORT);
      }
      else if (this.state.score == 1) {
        this.setState({
          myCalc: "2",
          score: 0
        })
      }
      else if (this.state.myCalc != 0 || this.state.myCalc.length >=2) {
        var numb1 = this.state.myCalc;
        var numb2 = "2";
        var numb3 = numb1 + numb2;
        this.setState({
          myCalc: numb3
        });
      }
      else if(this.state.myCalc == 0) {
        this.setState({
          myCalc: "2"
        })
      }
  	}
  	_but3 = () => {
  		if (this.state.myCalc.length == 16) {
        ToastAndroid.show('Przekraczasz zakres wyświetlacza', ToastAndroid.SHORT);
      }
      else if (this.state.score == 1) {
        this.setState({
          myCalc: "3",
          score: 0
        })
      }
      else if (this.state.myCalc != 0 || this.state.myCalc.length >=2) {
        var numb1 = this.state.myCalc;
        var numb2 = "3";
        var numb3 = numb1 + numb2;
        this.setState({
          myCalc: numb3
        });
      }
      else if (this.state.myCalc == 0) {
        this.setState({
          myCalc: "3"
        })
      }
  	}
  	_but4 = () => {
  		if (this.state.myCalc.length == 16) {
        ToastAndroid.show('Przekraczasz zakres wyświetlacza', ToastAndroid.SHORT);
      }
      else if (this.state.score == 1) {
        this.setState({
          myCalc: "4",
          score: 0
        })
      }
      else if (this.state.myCalc != 0 || this.state.myCalc.length >=2) {
        var numb1 = this.state.myCalc;
        var numb2 = "4";
        var numb3 = numb1 + numb2;
        this.setState({
          myCalc: numb3
        });
      }
      else if (this.state.myCalc == 0) {
        this.setState({
          myCalc: "4"
        })
      }
  	}
  	_but5 = () => {
  		if (this.state.myCalc.length == 16) {
        ToastAndroid.show('Przekraczasz zakres wyświetlacza', ToastAndroid.SHORT);
      }
      else if (this.state.score == 1) {
        this.setState({
          myCalc: "5",
          score: 0
        })
      }
      else if (this.state.myCalc != 0 || this.state.myCalc.length >=2) {
        var numb1 = this.state.myCalc;
        var numb2 = "5";
        var numb3 = numb1 + numb2;
        this.setState({
          myCalc: numb3
        });
      }
      else if (this.state.myCalc == 0) {
        this.setState({
          myCalc: "5"
        })
      }
  	}
  	_but6 = () => {
  		if (this.state.myCalc.length == 16) {
        ToastAndroid.show('Przekraczasz zakres wyświetlacza', ToastAndroid.SHORT);
      }
      else if (this.state.score == 1) {
        this.setState({
          myCalc: "6",
          score: 0
        })
      }
      else if (this.state.myCalc != 0 || this.state.myCalc.length >=2) {
        var numb1 = this.state.myCalc;
        var numb2 = "6";
        var numb3 = numb1 + numb2;
        this.setState({
          myCalc: numb3
        });
      }
      else if (this.state.myCalc == 0) {
        this.setState({
          myCalc: "6"
        })
      }
  	}
  	_but7 = () => {
  		if (this.state.myCalc.length == 16) {
        ToastAndroid.show('Przekraczasz zakres wyświetlacza', ToastAndroid.SHORT);
      }
      else if (this.state.score == 1) {
        this.setState({
          myCalc: "7",
          score: 0
        })
      }
      else if (this.state.myCalc != 0 || this.state.myCalc.length >=2) {
        var numb1 = this.state.myCalc;
        var numb2 = "7";
        var numb3 = numb1 + numb2;
        this.setState({
          myCalc: numb3
        });
      }
      else if (this.state.myCalc == 0) {
        this.setState({
          myCalc: "7"
        })
      }
  	}
  	_but8 = () => {
  		if (this.state.myCalc.length == 16) {
        ToastAndroid.show('Przekraczasz zakres wyświetlacza', ToastAndroid.SHORT);
      }
      else if (this.state.score == 1) {
        this.setState({
          myCalc: "8",
          score: 0
        })
      }
      else if (this.state.myCalc != 0 || this.state.myCalc.length >=2) {
        var numb1 = this.state.myCalc;
        var numb2 = "8";
        var numb3 = numb1 + numb2;
        this.setState({
          myCalc: numb3
        });
      }
      else if (this.state.myCalc == 0) {
        this.setState({
          myCalc: "8"
        })
      }
  	}
  	_but9 = () => {
  		if (this.state.myCalc.length == 16) {
        ToastAndroid.show('Przekraczasz zakres wyświetlacza', ToastAndroid.SHORT);
      }
      else if (this.state.score == 1) {
        this.setState({
          myCalc: "9",
          score: 0
        })
      }
      else if (this.state.myCalc != 0 || this.state.myCalc.length >=2) {
        var numb1 = this.state.myCalc;
        var numb2 = "9";
        var numb3 = numb1 + numb2;
        this.setState({
          myCalc: numb3
        });
      }
      else if (this.state.myCalc == 0) {
        this.setState({
          myCalc: "9"
        })
      }
  	}
  	_but0 = () => {
    	if (this.state.myCalc.length == 16) {
        ToastAndroid.show('Przekraczasz zakres wyświetlacza', ToastAndroid.SHORT);
      }
      else if (this.state.myCalc != 0 || this.state.myCalc.length >=2) {
    		var numb1 = this.state.myCalc;
    		var numb2 = "0";
    		var numb3 = numb1 + numb2;
    		this.setState({
    			myCalc: numb3
    		});
    	}
  	}
  	_butDot = () => {
  		if (this.state.dotcount < 1) {
  			var numb1 = this.state.myCalc;
    		var numb2 = ".";
    		var numb3 = numb1 + numb2;
    		this.setState({
    			myCalc: numb3,
    			dotcount: 1
    		});
    	}
  	}
  	_butClean = () => {
    	this.setState({
    			myCalc: "0",
    			number1: 0,
         		number2: 0,
         		action: 0,
         		dotcount: 0
    		});
  	}
  	_butDel = () => {
  		var numb3 = this.state.myCalc;
		  numb3 = numb3.slice(0, -1);
		  this.setState({
    			myCalc: numb3
    		});
  	}
  	_butMemClean = () => {
  		this.setState({
  			memory: 0,
        myCalc: "0"
  		})
  		console.log(this.state.memory);
  	}
  	_butMemRetrieve = () => {
  		var numb3 = this.state.memory;
  		this.setState({
  			myCalc: numb3
  		})
  	}
  	_butMemAdd = () => {
  		var numb3 = this.state.myCalc;
  		console.log("zmienna " + numb3);
  		this.setState({
  			memory: numb3
  		})
  		console.log(this.state.memory);
  	}
  	_butChangeToken = () => {
      var numb1 = parseFloat(this.state.myCalc);
      var numb2 = numb1 * (-1);
      var numb3 = numb2.toString()
      this.setState({
          myCalc: numb3
      });
    }
  	_butAddPercent = () => {
      var numb1 = parseFloat(this.state.myCalc);
      var numb2 = numb1 / 100;
      var numb3 = numb2.toString()
      this.setState({
          myCalc: numb3
      });
    }
  	_butActionDivide = () => {
  		this.setState({
  			action: "/"
  		})
  		if (this.state.number1 == 0) {
  			var numb1 = parseFloat(this.state.myCalc);
  			this.setState({
  				number1: numb1
  			})
  			this.setState({
  				myCalc: 0,
  				dotcount: 0,
  				score: 0
  			})
  		}
  		if (this.state.number1 != 0) {
  			var numb1 = parseFloat(this.state.number1);
  			var numb2 = parseFloat(this.state.myCalc);
  			var numb3 = numb1/numb2;
        var numb4 = numb3.toString();
        var numb5;
        if (numb4.length > 16) {
             numb5 = Math.round(parseFloat(numb4) * 100000000000000) / 100000000000000;
          numb5 = numb5.toString();
          this.setState({
            myCalc: numb5
          })
          this.setState({
            number1: 0,
            number2: 0,
            action:0,
            score: 1  
          })
          ToastAndroid.show('Przekraczasz zakres wyświetlacza, liczba została zaokrąglona', ToastAndroid.SHORT);
        }
        else if (numb4.length < 16) {
          this.setState({
            myCalc: numb3
          })
          this.setState({
            number1: 0,
            number2: 0,
            action:0,
            score: 1  
          })
        }
  		}
  		}
  	_butActionMultiply = () => {
  		this.setState({
  			action: "*"
  		})
  		if (this.state.number1 == 0) {
  			var numb1 = parseFloat(this.state.myCalc);
  			this.setState({
  				number1: numb1
  			})
  			this.setState({
  				myCalc: 0,
  				dotcount: 0,
  				score: 0
  			})
  		}
  		if (this.state.number1 != 0) {
  			var numb1 = parseFloat(this.state.number1);
  			var numb2 = parseFloat(this.state.myCalc);
  			var numb3 = numb1*numb2;
  			var numb4 = numb3.toString();
        var numb5;
        if (numb4.length >= 16) {
            numb5 = Math.round(parseFloat(numb4) * 100000000000000) / 100000000000000;
          numb5 = numb5.toString();
          this.setState({
            myCalc: numb5
          })
          this.setState({
            number1: 0,
            number2: 0,
            action:0,
            score: 1  
          })
          ToastAndroid.show('Przekraczasz zakres wyświetlacza, liczba została zaokrąglona', ToastAndroid.SHORT);
        }
        else if (numb4.length < 16) {
          this.setState({
            myCalc: numb3
          })
          this.setState({
            number1: 0,
            number2: 0,
            action:0,
            score: 1  
          })
        }
  		}
  		}
  	_butActionSubstraction = () => {
  		this.setState({
  			action: "-"
  		})
  		if (this.state.number1 == 0) {
  			var numb1 = parseFloat(this.state.myCalc);
  			this.setState({
  				number1: numb1
  			})
  			this.setState({
  				myCalc: 0,
  				dotcount: 0,
  				score: 0
  			})
  		}
  		if (this.state.number1 != 0) {
  			var numb1 = parseFloat(this.state.number1);
  			var numb2 = parseFloat(this.state.myCalc);
  			var numb3 = numb1-numb2;
  			var numb4 = numb3.toString();
        if (numb4.length > 16) {
            ToastAndroid.show('Przekraczasz zakres wyświetlacza', ToastAndroid.SHORT);
        }
        else if (numb4.length < 16) {
          this.setState({
            myCalc: numb3
          })
          this.setState({
            number1: 0,
            number2: 0,
            action:0,
            score: 1  
          })
        }
  		}
  		}
  	_butActionAddition = () => {
  		this.setState({
  			action: "+"
  		})
  		if (this.state.number1 == 0) {
  			var numb1 = parseFloat(this.state.myCalc);
  			this.setState({
  				number1: numb1
  			})
  			this.setState({
  				myCalc: 0,
  				dotcount: 0,
  				score: 0
  			})
  		}
  		if (this.state.number1 != 0) {
  			var numb1 = parseFloat(this.state.number1);
  			var numb2 = parseFloat(this.state.myCalc);
  			var numb3 = numb1+numb2;
  			var numb4 = numb3.toString();
        if (numb4.length > 16) {
            ToastAndroid.show('Przekraczasz zakres wyświetlacza', ToastAndroid.SHORT);
        }
        else if (numb4.length < 16) {
          this.setState({
            myCalc: numb3
          })
          this.setState({
            number1: 0,
            number2: 0,
            action:0,
            score: 1  
          })
        }
  		}
  		}
  	_butActionScore = () => {
  			var numbA = this.state.action
  			switch (numbA) {
  				case "+":
		  			var numb1 = parseFloat(this.state.number1);
		  			var numb2 = parseFloat(this.state.myCalc);
		  			var numb3 = numb1+numb2;
		  			var numb4 = numb3.toString();
        if (numb4.length > 16) {
            ToastAndroid.show('Przekraczasz zakres wyświetlacza', ToastAndroid.SHORT);
            this.setState({
              myCalc: 0,
            })
            this.setState({
              number1: 0,
              number2: 0,
              action:0,
              score: 1,  
            })
        }
        else if (numb4.length < 16) {
          this.setState({
            myCalc: numb3
          })
          this.setState({
            number1: 0,
            number2: 0,
            action:0,
            score: 1  
          })
        }
  					break;
  				case "-":
		  			var numb1 = parseFloat(this.state.number1);
		  			var numb2 = parseFloat(this.state.myCalc);
		  			var numb3 = numb1-numb2;
		  			var numb4 = numb3.toString();
        if (numb4.length > 16) {
            ToastAndroid.show('Przekraczasz zakres wyświetlacza', ToastAndroid.SHORT);
            this.setState({
              myCalc: 0,
            })
            this.setState({
              number1: 0,
              number2: 0,
              action:0,
              score: 1,  
            })
        }
        else if (numb4.length < 16) {
          this.setState({
            myCalc: numb3
          })
          this.setState({
            number1: 0,
            number2: 0,
            action:0,
            score: 1  
          })
        }
  					break;
  				case "*":
		  			var numb1 = parseFloat(this.state.number1);
		  			var numb2 = parseFloat(this.state.myCalc);
		  			var numb3 = numb1*numb2;
		  			var numb4 = numb3.toString();
            var numb5;
        if (numb4.length > 16) {
          numb5 = Math.round(parseFloat(numb4) * 100000000000000) / 100000000000000;
          numb5 = numb5.toString();
          this.setState({
            myCalc: numb5
          })
          this.setState({
            number1: 0,
            number2: 0,
            action:0,
            score: 1  
          })
          ToastAndroid.show('Przekraczasz zakres wyświetlacza, liczba została zaokrąglona', ToastAndroid.SHORT);
        }
        else if (numb4.length < 16) {
          this.setState({
            myCalc: numb3
          })
          this.setState({
            number1: 0,
            number2: 0,
            action:0,
            score: 1  
          })
        }
  					break;
  				case "/":
		  			var numb1 = parseFloat(this.state.number1);
		  			var numb2 = parseFloat(this.state.myCalc);
		  			var numb3 = numb1/numb2;
		  			this.setState({
		  				myCalc: numb3
		  			})
		  			var numb4 = numb3.toString();
        if (numb4.length >= 16) {
             numb5 = Math.round(parseFloat(numb4) * 100000000000000) / 100000000000000;
          numb5 = numb5.toString();
          this.setState({
            myCalc: numb5
          })
          this.setState({
            number1: 0,
            number2: 0,
            action:0,
            score: 1  
          })
          ToastAndroid.show('Przekraczasz zakres wyświetlacza, liczba została zaokrąglona', ToastAndroid.SHORT);
        }
        else if (numb4.length < 16) {
          this.setState({
            myCalc: numb3
          })
          this.setState({
            number1: 0,
            number2: 0,
            action:0,
            score: 1  
          })
        }
  					break;
  				default:
  					// statements_def
  					break;
  			}
  		}
  	

	render() {
		return (
			<View style={{flex: 1, backgroundColor: "white", paddingTop: 60}}>
      <DoubleClick onClick={this.handleDblClick}>
			<View style={styles.calcarea}> 
      <Text style={styles.displayText}>{this.state.myCalc}</Text>
      </View>
      </DoubleClick>
			<View style={styles.columns}>
				<View style={styles.doublecol}>
					<View style={{flex: 1, flexDirection: "row"}}>
						<View id="firstcolumn">
							<Button
								title="C"
								titleStyle={styles.buttonText}
						buttonStyle={styles.styledbutton}
								containerStyle={{ marginTop: 0 }}
								onPress={this._butClean}
							/>
							<Button
								title=""
								icon={
									<Icon name="md-backspace" size={15} color="white" />
								}
								titleStyle={styles.buttonText}
						buttonStyle={styles.styledbutton}
								containerStyle={{ marginTop: 0 }}
								onPress={this._butDel}
							/>
							<Button
								title="7"
								titleStyle={styles.buttonText}
						buttonStyle={styles.styledbutton}
								containerStyle={{ marginTop: 0 }}
								onPress={this._but7}
							/>
							<Button
								title="4"
								titleStyle={styles.buttonText}
						buttonStyle={styles.styledbutton}
								containerStyle={{ marginTop: 0 }}
								onPress={this._but4}
							/>
							<Button
								title="1"
								titleStyle={styles.buttonText}
						buttonStyle={styles.styledbutton}
								containerStyle={{ marginTop: 0 }}
								onPress={this._but1}
							/>
								
						</View>
						<View id="secondcolumn">
							<Button
								title="MC"
								titleStyle={styles.buttonText}
						    buttonStyle={styles.styledbutton}
								containerStyle={{ marginTop: 0 }}
								onPress={this._butMemClean}
							/>
							<Button
								title="±"
								titleStyle={styles.buttonText}
						    buttonStyle={styles.styledbutton}
								containerStyle={{ marginTop: 0 }}
                onPress={this._butChangeToken}
							/>
							<Button
								title="8"
								titleStyle={styles.buttonText}
						    buttonStyle={styles.styledbutton}
								containerStyle={{ marginTop: 0 }}
								onPress={this._but8}
							/>
							<Button
								title="5"
								titleStyle={styles.buttonText}
						    buttonStyle={styles.styledbutton}
								containerStyle={{ marginTop: 0 }}
								onPress={this._but5}
							/>
							<Button
								title="2"
								titleStyle={styles.buttonText}
						buttonStyle={styles.styledbutton}
								containerStyle={{ marginTop: 0 }}
								onPress={this._but2}
							/>
						</View>
					</View>
					<View>
					<Button
						title="0"
						titleStyle={styles.buttonText}
						buttonStyle={styles.styled0}
						containerStyle={{ paddingTop: 0 }}
						onPress={this._but0}
					/>
					</View>
				</View>
				<View id="thirdcolumn">
					<Button
						title="MR"
						titleStyle={styles.buttonText}
						buttonStyle={styles.styledbutton}
						containerStyle={{ marginTop: 0 }}
						onPress={this._butMemRetrieve}
					/>
					<Button
						title="%"
						titleStyle={styles.buttonText}
						buttonStyle={styles.styledbutton}
						containerStyle={{ marginTop: 0 }}
            onPress={this._butAddPercent}
					/>
					<Button
						title="9"
						titleStyle={styles.buttonText}
						buttonStyle={styles.styledbutton}
						containerStyle={{ marginTop: 0 }}
						onPress={this._but9}
					/>
					<Button
						title="6"
						titleStyle={styles.buttonText}
						buttonStyle={styles.styledbutton}
						containerStyle={{ marginTop: 0 }}
						onPress={this._but6}
					/>
					<Button
						title="3"
						titleStyle={styles.buttonText}
						buttonStyle={styles.styledbutton}
						containerStyle={{ marginTop: 0 }}
						onPress={this._but3}
					/>
					<Button
						title="."
						titleStyle={styles.buttonText}
						buttonStyle={styles.styledbutton}
						containerStyle={{ marginTop: 0 }}
						onPress={this._butDot}
					/>
				</View>
				<View id="fourthcolumn">
					<Button
						title="M+"
						titleStyle={styles.buttonText}
						buttonStyle={styles.styledbutton}
						containerStyle={{ marginTop: 0 }}
						onPress = {this._butMemAdd}
					/>
					<Button
						title="/"
						titleStyle={styles.buttonText}
						buttonStyle={styles.styledbutton}
						containerStyle={{ marginTop: 0 }}
						onPress={this._butActionDivide}
					/>
					<Button
						title="*"
					titleStyle={styles.buttonText}
						buttonStyle={styles.styledbutton}
						containerStyle={{ marginTop: 0 }}
						onPress={this._butActionMultiply}
					/>
					<Button
						title="-"
						titleStyle={styles.buttonText}
						buttonStyle={styles.styledbutton}
						containerStyle={{ marginTop: 0 }}
						onPress={this._butActionSubstraction}
					/>
					<Button
						title="+"
						titleStyle={styles.buttonText}
						buttonStyle={styles.styledbutton}
						containerStyle={{ marginTop: 0 }}
						onPress={this._butActionAddition}
					/>
					<Button
						title="="
						titleStyle={styles.buttonText}
						buttonStyle={styles.styledbutton}
						containerStyle={{ marginTop: 0 }}
						onPress={this._butActionScore}
					/>
				</View>
			</View>
			</View>
		);
	}
}

AppRegistry.registerComponent('AwesomeProject', () => CalculatorScreen);
const styles = StyleSheet.create({
	styledbutton:{
		backgroundColor: "rgba(92, 99,216, 1)",
		width: 86,
		height: 86
	},
	styled0:{
		backgroundColor: "rgba(92, 99,216, 1)",
		width: 169.5,
		height: 86,
    position: "absolute",
    marginTop: -139
	},
	buttonText:{
		fontWeight: "600",
    fontSize: 25,
	},
	calcarea:{
		paddingRight: 10,
    backgroundColor: "white",
    alignItems: "flex-end",
	},
	columns:{
		flex: 1,
	 	flexDirection: "row",
	 	marginLeft: 22,
    marginRight: 22, 
	},
	doublecol:{
		flex: 1,
		flexDirection: "column",
    padding: 0,
  },
  displayText:{
    fontSize: 40,
  },
});